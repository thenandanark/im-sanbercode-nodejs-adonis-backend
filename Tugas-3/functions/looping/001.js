console.log("LOOPING PERTAMA");

a = 2
while (a <= 20){
    console.log(a + " - I love coding")
    a = a + 2;
}

console.log("LOOPING KEDUA");

a = 20
while (a >= 1){
    console.log(a + " - I will become a mobile developer")
    a = a - 2;
}
