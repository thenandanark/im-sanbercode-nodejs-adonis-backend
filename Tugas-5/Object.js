console.log("Soal No.1")

function arrayToObject(arr){
    for(var i = 0; i < arr.length; i++ ){
        var thisYear = (new Date()).getFullYear();

        var personArr = arr[i];

        var personObj = {
            firstname : personArr[0],
            lastname : personArr[1],
            gender : personArr[2],
            age : personArr[3],
        }

        if(!personArr[3] || personArr[3] > thisYear){
            personObj.age = "invalid birthyear"
        }else{
            personObj.age = thisYear - personArr[3]
        }

        var fullname = personObj.firstname + " " + personObj.lastname
        console.log(`${i + 1} . ${fullname} : `, personObj)
    }
}
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people)


console.log("")
console.log("Soal No.2")

function naikAngkot(arrPenumpang){
    var rute = ['A', 'B', 'C', 'D', 'E', 'F'];

    var output = []

    for(var a = 0; a < arrPenumpang.length; a++){
        var penumpangSekarang = arrPenumpang[a]
        var obj = {
            arrPenumpang: penumpangSekarang[0],
            Naik : penumpangSekarang[1],
            Tujuan : penumpangSekarang[2],
        }
        var bayar = (rute.indexOf(penumpangSekarang[2]) - rute.indexOf(penumpangSekarang[1])) * 2000 

        obj.bayar = bayar

        output.push(obj)
    }
    return output
}

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]))

console.log("")
console.log("Soal No.3")

function nilaiTertinggi(siswa){
    var output = {}
    for(var b = 0; b < siswa.length; b++){
        var peringkat = siswa[b];
        if(!output[peringkat.class]){
            output[peringkat.class]
        }else{
            if(peringkat.score > output[peringkat.class].score){

            }
        }
    }
    return output
}

console.log(nilaiTertinggi([
    {
      name: 'Asep',
      score: 90,
      class: 'adonis'
    },
    {
      name: 'Ahmad',
      score: 85,
      class: 'vuejs'
    },
    {
      name: 'Regi',
      score: 74,
      class: 'adonis'
    },
    {
      name: 'Afrida',
      score: 78,
      class: 'reactjs'
    }
  ]))

  console.log(nilaiTertinggi([
    {
      name: 'Bondra',
      score: 100,
      class: 'adonis'
    },
    {
      name: 'Putri',
      score: 76,
      class: 'laravel'
    },
    {
      name: 'Iqbal',
      score: 92,
      class: 'adonis'
    },
    {
      name: 'Tyar',
      score: 71,
      class: 'laravel'
    },
    {
      name: 'Hilmy',
      score: 80,
      class: 'vuejs'
    }
  ]))