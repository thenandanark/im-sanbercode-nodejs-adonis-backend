console.log("Soal no.1")
console.log("")

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

function datahandling(a){
    for(var i = 0; i < a.length; i++){
        console.log("Nomor ID: ", a[i][0])
        console.log("Nama Lengkap: ", a[i][1])
        console.log("TTL: ", a[i][2], a[i][3])
        console.log("Hobi: ", a[i][4])
        console.log("")
    }
}

datahandling(input)

console.log("")
console.log("Soal No.2")
console.log("")

function balikKata(string){
    var output = ""
    var b = string.length

    for(var i = b - 1; i >= 0; i--){
        output += string[i]
    }
    return output;
}

console.log(balikKata("SanberCode")) 
//Output: edoCrebnaS

console.log(balikKata("racecar")) 
//Output: racecar

console.log(balikKata("kasur rusak"))
//Output: kasur rusak

console.log(balikKata("haji ijah"))
//Output: haji ijah

console.log(balikKata("I am Sanbers"))
//Output: srebnaS ma I