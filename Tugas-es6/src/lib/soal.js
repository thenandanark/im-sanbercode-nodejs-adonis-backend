export const sapa = (nama) => {
    return `halo selamat pagi, ${nama}`
}

export const bio = (nickname, domisili, umur) => {
    return {
        nickname,
        domisili,
        umur
    }
}


const data = [
    { name: "Ahmad", kelas: "adonis"},
    { name: "Regi", kelas: "laravel"},
    { name: "Bondra", kelas: "adonis"},
    { name: "Iqbal", kelas: "vuejs" },
    { name: "Putri", kelas: "Laravel" }
    ]

export const filterdata = (kelas) => {
    for(let i=0; i<data.length; i++){
        return data.filter((el) => el['kelas'].toLowerCase().includes(kelas.toLowerCase()));
    }
}