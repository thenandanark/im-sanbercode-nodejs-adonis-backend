class Tags{
    constructor(tags){
        this._tag = tags
    }
    get tag(){
        return this._tag
    }
    set tag(tag){
        this._tag = tag
    }
}
export default Tags;