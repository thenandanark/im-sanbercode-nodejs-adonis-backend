import Kelas from "./kelas"

class BootCamp{
    constructor(name){
        this._name = name
        this._tags = []
    }
    get name(){
        return this._name
    }
    set name(name){
        this._name = name
    }

    get tags(){
        return this._tags
    }

    createKelas(name, level, instructor){
        let newKelas = new Kelas(name, level, instructor)
        this._tags.push(newKelas)
    }

    insertTag(namaKelas, objTag){
        let carikelas = this._tags.find((cari) => cari.name === namaKelas)
        carikelas.addTags(objTag)
    }
}

export default BootCamp;