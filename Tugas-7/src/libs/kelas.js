class Kelas{
    constructor(name, level, instructor){
        this._name = name
        this._level = level
        this._instructor = instructor
        this._tags = []
    }
    get name(){
        return this._name
    }
    set name(strname){
        this._name = strname
    }

    get level(){
        return this._level
    }
    set level(strlevel){
        this._level = strlevel
    }

    get instructor(){
        return this._instructor
    }
    set instructor(strinstructor){
        this._instructor = strinstructor
    }

    get tags(){
        return this._tags
    }

    addTags(objTag){
        this._tags.push(objTag)
    }
}

export default Kelas;