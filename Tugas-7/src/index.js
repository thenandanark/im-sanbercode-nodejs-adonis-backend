import BootCamp from "./libs/bootcamp";
import Tags from "./libs/tags";

const bootcamp = new BootCamp("public");

bootcamp.createKelas("laravel", "Beginner", "Abduh")
bootcamp.createKelas("NodeJS", "Beginner", "Abdul")

console.log(bootcamp.tags)

let tagslist = ["tag1", "tag2", "tag3", "tag4", "tag5", "tag6"]

tagslist.map((tag, index) => {
    let newTag = new Tags(tag)
    let kelas = bootcamp.kelas[index % 2].name
    bootcamp.insertTag(kelas, newTag)
})

bootcamp.kelas.forEach(cari => {
    console.log(cari)
});

