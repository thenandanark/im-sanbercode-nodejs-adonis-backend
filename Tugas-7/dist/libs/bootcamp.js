"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _kelas = _interopRequireDefault(require("./kelas"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

var BootCamp = /*#__PURE__*/function () {
  function BootCamp(name) {
    _classCallCheck(this, BootCamp);

    this._name = name;
    this._tags = [];
  }

  _createClass(BootCamp, [{
    key: "name",
    get: function get() {
      return this._name;
    },
    set: function set(name) {
      this._name = name;
    }
  }, {
    key: "tags",
    get: function get() {
      return this._tags;
    }
  }, {
    key: "createKelas",
    value: function createKelas(name, level, instructor) {
      var newKelas = new _kelas["default"](name, level, instructor);

      this._tags.push(newKelas);
    }
  }, {
    key: "insertTag",
    value: function insertTag(namaKelas, objTag) {
      var carikelas = this._tags.find(function (cari) {
        return cari.name === namaKelas;
      });

      carikelas.addTags(objTag);
    }
  }]);

  return BootCamp;
}();

var _default = BootCamp;
exports["default"] = _default;