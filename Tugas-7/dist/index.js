"use strict";

var _bootcamp = _interopRequireDefault(require("./libs/bootcamp"));

var _tags = _interopRequireDefault(require("./libs/tags"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var bootcamp = new _bootcamp["default"]("public");
bootcamp.createKelas("laravel", "Beginner", "Abduh");
bootcamp.createKelas("NodeJS", "Beginner", "Abdul");
console.log(bootcamp.tags);
var tagslist = ["tag1", "tag2", "tag3", "tag4", "tag5", "tag6"];
tagslist.map(function (tag, index) {
  var newTag = new _tags["default"](tag);
  var kelas = bootcamp.kelas[index % 2].name;
  bootcamp.insertTag(kelas, newTag);
});
bootcamp.kelas.forEach(function (cari) {
  console.log(cari);
});